import java.util.Arrays;

public class StringReader {
    public StringReader() {
    }

    public int[] decipherCoord(String command) {
        String lCommand = command.toLowerCase();
        int[] returnArr = new int[2];
        returnArr[0] = lCommand.charAt(0) - 'a';
        returnArr[1] = Character.getNumericValue(lCommand.charAt(1)) - 1;
        return returnArr;
    }


    public int[] decipherMove(String command) {
        int[] returnArr = new int[4];
        command = command.toLowerCase();
        returnArr[0] = command.charAt(0) - 'a';
        returnArr[1] = command.charAt(1) - '1';
        returnArr[2] = command.charAt(3) - 'a';
        returnArr[3] = command.charAt(4) - '1';
        return returnArr;
    }

    public PieceEnum decipherPiece(String piece) {
        switch (piece) {
            case "BBishop":
                return PieceEnum.BBishop;
            case "BPawn":
                return PieceEnum.BPawn;
            case "BQueen":
                return PieceEnum.BQueen;
            case "BKnight":
                return PieceEnum.BKnight;
            case "BKing":
                return PieceEnum.BKing;
            case "BRook":
                return PieceEnum.BRook;
            case "WBishop":
                return PieceEnum.WBishop;
            case "WQueen":
                return PieceEnum.WQueen;
            case "WKnight":
                return PieceEnum.WKnight;
            case "WKing":
                return PieceEnum.WKing;
            case "WRook":
                return PieceEnum.WRook;
            case "WPawn":
                return PieceEnum.WPawn;
            default:
                return PieceEnum.NONE;

        }
    }
}
