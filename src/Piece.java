public class Piece {
    protected int possibleMoves;
    protected int xCoord;
    protected int yCoord;
    protected boolean isPinned;
    protected Piece revertDestPiece;
    protected BKing bking;
    protected WKing wking;
    protected String team;
    protected boolean hasMoved;
    /**
     * Valid Moves Memo,
     * -2 If Enemy
     * -1 if Allied
     * 1 if movable
     * 0 if out of basic move range
     */
    protected int[][] validMoves;
    protected Piece[][] boardStatus;

    public Piece(int xCoord, int yCoord, String team, Piece[][] boardStatus) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        hasMoved = false;
        this.team = team;
        validMoves = new int[8][8];
        this.boardStatus = boardStatus;
        isPinned = false;
    }

    public boolean move(int xMove, int yMove) {
        hasMoved = true;
        xCoord = xMove;
        yCoord = yMove;
        return true;
    }

    public void tempMove(int xMove, int yMove) {
        xCoord = xMove;
        yCoord = yMove;
    }

    public boolean canMove(int xMove, int yMove) {
        return validMoves[xMove][yMove] == 1;
    }

    public void setCoord(int setX, int setY) {
        xCoord = setX;
        yCoord = setY;
    }

    public int[][] getValidMoves() {
        return validMoves;
    }

    public int getPossibleMoves() {
        return possibleMoves;
    }

    public int getXCoord() {
        return xCoord;
    }

    public int getYCoord() {
        return yCoord;
    }

    public boolean hasMoved() {
        return hasMoved;
    }

    public String getTeam() {
        return team;
    }

    /*public void calcIsPinned() {
        boardStatus[yCoord][xCoord] = null;
        if (team.equals("W")) {
            isPinned = wking.isUnderCheck();
        } else {
            isPinned = bking.isUnderCheck();
        }
        revertBoard();
    }*/

    public void revertBoard(int moveXCoord, int moveYCoord) {
        boardStatus[yCoord][xCoord] = this;
        boardStatus[moveYCoord][moveXCoord] = revertDestPiece;
    }

    public void tempMovePiece(int moveXCoord, int moveYCoord) {
        revertDestPiece = boardStatus[moveYCoord][moveXCoord];
        boardStatus[moveYCoord][moveXCoord] = this;
        boardStatus[yCoord][xCoord] = null;
    }

    public boolean moveLeaveKingCheck(int moveXCoord, int moveYCoord) {
        boolean temp;
        int originX = xCoord;
        int originY = yCoord;
        if (this instanceof WKing || this instanceof BKing) {
            tempMove(moveXCoord, moveYCoord);
        }
        tempMovePiece(moveXCoord, moveYCoord);
        if (team.equals("W")) {
            temp = wking.isUnderCheck();
        } else {
            temp = bking.isUnderCheck();
        }
        revertBoard(moveXCoord, moveYCoord);
        if (this instanceof WKing || this instanceof BKing) {
            tempMove(originX, originY);
        }
        return temp;
    }

    public void calcValidMoves() {
        resetValidMoves();
        //gonna be overriden by child class
    }

    public void resetValidMoves() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                validMoves[i][j] = 0;
            }
        }
    }

    public void printValidMoves() {
        for (int i = 7; i >= 0; --i) {
            for (int j = 0; j < 8; ++j) {
                System.out.print(validMoves[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void giveWKingRef() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                if (boardStatus[i][j] instanceof WKing) {
                    wking = (WKing) boardStatus[i][j];
                    break;
                }
            }
        }
    }

    public void giveBkingRef() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                if (boardStatus[i][j] instanceof BKing) {
                    bking = (BKing) boardStatus[i][j];
                    break;
                }
            }
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        Piece temp;
        if (obj instanceof Piece) {
            temp = (Piece) obj;
        } else {
            return false;
        }

        return (temp.getXCoord() == getXCoord()) && (temp.getYCoord() == getYCoord());
    }

    protected boolean indexInRange(int xCoord, int yCoord) {
        return (xCoord >= 0 && xCoord < 8 && yCoord >= 0 && yCoord < 8);
    }
}
