import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.HashMap;
import java.io.IOException;

public class ClientThread extends Thread {
    private Socket clientSocket;
    private Socket opponentSocket;

    private ServerSocket serverSocket;
    private HashMap<Integer, Match> gameHM;
    private HashMap<Socket, Match> gameSocketMatch;
    private Match match;
    private int gameCode;
    private BufferedReader br;
    private PrintStream ps;

    public ClientThread(Socket clientSocket, ServerSocket serverSocket, HashMap<Integer, Match> gameHM, HashMap<Socket, Match> gameSocketMatch) {
        try {
            br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            ps = new PrintStream(clientSocket.getOutputStream());
            this.clientSocket = clientSocket;
            this.serverSocket = serverSocket;
            this.gameHM = gameHM;
            this.gameSocketMatch = gameSocketMatch;

            match = null;


            run();
        } catch (IOException e) {
            System.out.println("ERROR CLIENTTHREAD");
        }
    }

    @Override
    public void run() {
        while (!currentThread().isInterrupted()) {

        }
        closeConnection();
        removeAndTerminate();
    }

    public void commandReceived(String cmd) throws IncorrectPromotionPiece{
        String[] cmdArr = cmd.split("/", -1);
        String cmdType = cmdArr[0];
        switch (cmdType) {
            case ("MOVE"):// FROM AND TO
                if(match.getBoard().movePiece(cmdArr[1])){
                    ps.println("MOVED");
                }
                else{
                    ps.println("FAILED");
                }

                break;
            case ("CREATE"):// gamecode/team/name/initialtime/timeincrement
                if(!gameHM.containsKey(cmdArr[1])) {
                    Match match = new Match(Integer.valueOf(cmdArr[1]), Double.valueOf(cmdArr[4]), Double.valueOf(cmdArr[5]));
                    this.gameCode = Integer.valueOf(cmdArr[1]);
                    gameHM.put(Integer.valueOf(cmdArr[1]), match);
                    gameSocketMatch.put(clientSocket, match);

                    if(cmdArr[2].equals("W")){
                        match.setWhiteSocket(clientSocket);
                        match.setWhiteName(cmdArr[3]);
                    }
                    else{
                        match.setBlackSocket(clientSocket);
                        match.setBlackName(cmdArr[3]);
                    }
                    match.incrementPlayerCount();

                    ps.println("CREATED");
                }
                else{
                    ps.println("FAILED");
                }

            break;
            case ("REMATCH"):
                break;

            case ("JOIN"):// gamecode/name
                if(!gameHM.containsKey(cmdArr[1])){

                    if(match.getWhiteSocket() != null){
                        match.setBlackSocket(clientSocket);
                        match.setBlackName(cmdArr[2]);
                    }
                    else{
                        match.setWhiteSocket(clientSocket);
                        match.setWhiteName(cmdArr[2]);
                    }



                    match.incrementPlayerCount();
                    ps.println("JOINED");
                }
                else{
                    ps.println("FAILED");
                }
                break;
            case("JOIN ABANDONED GAME"):
                break;

        }
    }

    public void setGameHM(HashMap<Integer, Match> gameHM) {
        this.gameHM = gameHM;
    }

    public void setGameSocketMatch(HashMap<Socket, Match> gameSocketMatch) {
        this.gameSocketMatch = gameSocketMatch;
    }

    public Match newMatch(int gameCode) {
        if (gameHM.containsKey(gameCode)) {
            return null;
        }
        Match tempMatch = new Match(gameCode);

        gameHM.put(gameCode, new Match(gameCode));
        return tempMatch;
    }


    public void closeConnection() {
        try {
            clientSocket.close();
            this.interrupt();
        } catch (IOException e) {
            System.out.println("ERROR CLOSECONNECTION");
        }
    }

    public void removeAndTerminate() {
        gameHM.remove(gameCode);
        gameSocketMatch.remove(clientSocket);
    }
}
