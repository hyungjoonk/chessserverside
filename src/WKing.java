public class WKing extends Piece{

    private int[][] validCoordRef;
    public WKing(int xCoord, int yCoord, Piece[][] boardStatus){
        super(xCoord, yCoord, "W", boardStatus);
        int count = 0;
        validCoordRef = new int[8][2];
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j < 3; ++j){
                if(i == 1 && j == 1){
                    continue;
                }
                validCoordRef[count][0] = i - 1;
                validCoordRef[count][1] = j - 1;
                ++count;
            }
        }
        wking = this;
    }

    @Override
    public void calcValidMoves(){
        possibleMoves = 0;
        resetValidMoves();
        Piece pieceAtDest;
        int xAdd;
        int yAdd;
        for(int i = 0; i < 8; ++i){
            xAdd = validCoordRef[i][0];
            yAdd = validCoordRef[i][1];
            if(!indexInRange(xCoord + xAdd, yCoord + yAdd)){
                continue;
            }
            pieceAtDest = boardStatus[yCoord + yAdd][xCoord + xAdd];
            if(pieceAtDest == null || (pieceAtDest != null && !pieceAtDest.team.equals(team))){
                if(!moveLeaveKingCheck(xCoord + xAdd, yCoord + yAdd)){
                    validMoves[yCoord + yAdd][xCoord + xAdd] = 1;
                    ++possibleMoves;
                }
            }
        }
        if(canLongCastle()){
            validMoves[0][2] = 1;
            ++possibleMoves;
        }
        if(canShortCastle()){
            validMoves[0][6] = 1;
            ++possibleMoves;
        }
    }

    public boolean isUnderCheck(){
        Piece objAtCheck;
        int tempXCoord = xCoord;
        int tempYCoord = yCoord;

        ++tempYCoord;
        ++tempXCoord;
        if(indexInRange(tempYCoord, tempXCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BPawn) {
            return true;
        }
        for(; indexInRange(tempXCoord, tempYCoord); ++tempXCoord, ++tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof BBishop || objAtCheck instanceof BQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        ++tempYCoord;
        --tempXCoord;
        if(indexInRange(tempYCoord, tempXCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BPawn) {
            return true;
        }
        for(; indexInRange(tempXCoord, tempYCoord); --tempXCoord, ++tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof BBishop || objAtCheck instanceof BQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        --tempYCoord;
        --tempXCoord;
        for(; indexInRange(tempXCoord, tempYCoord); --tempXCoord, --tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof BBishop || objAtCheck instanceof BQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;

        --tempYCoord;
        ++tempXCoord;
        for(; indexInRange(tempXCoord, tempYCoord); ++tempXCoord, --tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof BBishop || objAtCheck instanceof BQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        //ROOK CHECK

        ++tempXCoord;
        for(; indexInRange(tempXCoord, tempYCoord); ++tempXCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof BRook || objAtCheck instanceof BQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        --tempXCoord;
        for(; indexInRange(tempXCoord, tempYCoord); --tempXCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof BRook || objAtCheck instanceof BQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        ++tempYCoord;
        for(; indexInRange(tempXCoord, tempYCoord); ++tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof BRook || objAtCheck instanceof BQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        --tempYCoord;
        for(; indexInRange(tempXCoord, tempYCoord); --tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof BRook || objAtCheck instanceof BQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }

        //KNIGHT PART

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord++;
        tempYCoord += 2;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord += 2;
        tempYCoord++;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord += 2;
        tempYCoord--;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord++;
        tempYCoord -= 2;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord--;
        tempYCoord -= 2;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord -= 2;
        tempYCoord--;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord -= 2;
        tempYCoord++;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord--;
        tempYCoord += 2;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof BKnight){
            return true;
        }
        return false;
    }

    public boolean canShortCastle(){
        if(boardStatus[0][4] instanceof WKing && !hasMoved && boardStatus[0][5] == null && boardStatus[0][6] == null &&
                boardStatus[0][7] instanceof WRook && !boardStatus[0][7].hasMoved){
            if(!isUnderCheck() && !moveLeaveKingCheck(5, 0) && !moveLeaveKingCheck(6, 0)){
                return true;
            }
        }
        return false;
    }

    public boolean canLongCastle(){
        if(boardStatus[0][4] instanceof WKing && !hasMoved && boardStatus[0][3] == null && boardStatus[0][2] == null && boardStatus[0][1] == null &&
                boardStatus[0][0] instanceof WRook && !boardStatus[0][0].hasMoved){
            if(!isUnderCheck() && !moveLeaveKingCheck(3, 0) && !moveLeaveKingCheck(2, 0)){
                return true;
            }
        }
        return false;
    }

    public String toString(){
        return "WKi";
    }
}
