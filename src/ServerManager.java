import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.HashMap;

public class ServerManager {
    private HashMap<Integer, Match> gameHM;
    private HashMap<Socket, Match> gameSocketMatch;
    private ServerSocket serverSocket;

    public ServerManager() throws IOException {
        gameHM = new HashMap<>();
        gameSocketMatch = new HashMap<>();
        serverSocket = new ServerSocket(49001);
        start();
        startServerThread();
    }

    public void startServerThread() {
        while (true) {
            Socket newClient;
            try {
                newClient = serverSocket.accept();
                new ClientThread(newClient, serverSocket, gameHM, gameSocketMatch).start();

            } catch (IOException e) {
                System.out.println("ERROR 1");
            }
        }
    }


    public void closeServer() throws IOException {
        serverSocket.close();
    }

    public void start() {


    }
    /*public Match newMatch(int gameCode) {
        if (gameHM.containsKey(gameCode)) {
            return null;
        }
        Match tempMatch = new Match(gameCode);

        gameHM.put(gameCode, new Match(gameCode));
        return tempMatch;
    }


    public void commandReceived(String cmd) {
        String[] cmdArr = cmd.split("/", 2);
        String cmdType = cmdArr[0];
        switch (cmdType) {
            case ("MOVE"):

                break;
            case ("CREATE"):
                Match match = newMatch(Integer.valueOf(cmdArr[1]));
                gameHM.put(Integer.valueOf(cmdArr[1]), match);
                gameSocketMatch.put()

        }
        break;
        case ("REMATCH"):
        break;

        case ("JOIN"):
        break;

    }*/
}
