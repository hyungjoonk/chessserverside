public class BPawn extends Piece {
    private int[] wPawnTwo;
    private int[] bPawnTwo;


    public BPawn(int xCoord, int yCoord, Piece[][] boardStatus, int[] wPawnTwo, int[] bPawnTwo) {
        super(xCoord, yCoord, "B", boardStatus);
        giveBkingRef();
        this.wPawnTwo = wPawnTwo;
        this.bPawnTwo = bPawnTwo;
    }


    @Override
    public boolean move(int xMoveCoord, int yMoveCoord) {
        if (yMoveCoord - yCoord == -2) {
            bPawnTwo[xMoveCoord] = 1;
        }
        return super.move(xMoveCoord, yMoveCoord);
    }

    @Override
    public void calcValidMoves() {
        /*if(bking.isUnderCheck()){
            calcUnderCheck();
            return;
        }*/
        possibleMoves = 0;
        resetValidMoves();
        Piece objAtDest;

        //Pawn going front
        if (indexInRange(xCoord, yCoord - 1) && !moveLeaveKingCheck(xCoord, yCoord - 1)) {
            if (indexInRange(yCoord - 1, xCoord)) {
                objAtDest = boardStatus[yCoord - 1][xCoord];
                if (objAtDest == null) {
                    validMoves[yCoord - 1][xCoord] = 1;
                    ++possibleMoves;
                }
            }
        }

        //Capturing Diagonal
        if (indexInRange(xCoord - 1, yCoord - 1) && !moveLeaveKingCheck(xCoord - 1, yCoord - 1)) {
            if (indexInRange(yCoord - 1, xCoord - 1)) {
                objAtDest = boardStatus[yCoord - 1][xCoord - 1];
                if (objAtDest != null && !(objAtDest.team.equals(this.team))) {
                    validMoves[yCoord - 1][xCoord - 1] = 1;
                    ++possibleMoves;
                }
                if (objAtDest == null && yCoord - 1 == 2 && wPawnTwo[xCoord - 1] == 1) {
                    validMoves[yCoord - 1][xCoord - 1] = 1;
                    ++possibleMoves;
                }
            }
        }

        if (indexInRange(xCoord + 1, yCoord - 1) && !moveLeaveKingCheck(xCoord + 1, yCoord - 1)) {
            if (indexInRange(yCoord - 1, xCoord + 1)) {
                objAtDest = boardStatus[yCoord - 1][xCoord + 1];
                if (objAtDest != null && !(objAtDest.team.equals(this.team))) {
                    validMoves[yCoord - 1][xCoord + 1] = 1;
                    ++possibleMoves;
                }
                if (objAtDest == null && yCoord - 1 == 2 && bPawnTwo[xCoord + 1] == 1) {
                    validMoves[yCoord - 1][xCoord + 1] = 1;
                    ++possibleMoves;
                }
            }
        }


        if (indexInRange(xCoord, yCoord - 2) && !moveLeaveKingCheck(xCoord, yCoord - 2)) {
            if (yCoord == 6 && boardStatus[yCoord - 1][xCoord] == null) {
                objAtDest = boardStatus[yCoord - 2][xCoord];
                if (objAtDest == null) {
                    validMoves[yCoord - 2][xCoord] = 1;
                    ++possibleMoves;
                }
            }
        }
    }

    /*private void calcUnderCheck(){
        possibleMoves = 0;
        resetValidMoves();
        Piece objAtDest;
        if(indexInRange(yCoord - 1, xCoord)) {
            objAtDest = boardStatus[yCoord - 1][xCoord];

            //forward
            if (objAtDest == null && !moveLeaveKingCheck(xCoord, yCoord - 1)) {
                validMoves[yCoord - 1][xCoord] = 1;
                ++possibleMoves;
                objAtDest = boardStatus[yCoord - 2][xCoord];
                if (objAtDest == null && yCoord == 1 && !moveLeaveKingCheck(xCoord, yCoord - 2)) {
                    validMoves[yCoord - 2][xCoord] = 1;
                    ++possibleMoves;
                }
            }
        }

        //diagonal
        if(indexInRange(xCoord - 1, yCoord - 1)) {
            objAtDest = boardStatus[yCoord - 1][xCoord - 1];
            if(objAtDest.team.equals("W") && !moveLeaveKingCheck(xCoord - 1, yCoord - 1)){
                validMoves[yCoord - 1][xCoord - 1] = 1;
                ++possibleMoves;
            }
        }

        if(indexInRange(xCoord + 1, yCoord - 1)){
            objAtDest = boardStatus[yCoord - 1][xCoord + 1];
            if(objAtDest.team.equals("W") && !moveLeaveKingCheck(xCoord - 1, yCoord - 1)){
                validMoves[yCoord - 1][xCoord + 1] = 1;
                ++possibleMoves;
            }
        }
    }*/


    public String toString() {
        return "BPa";
    }
}
