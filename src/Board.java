import java.util.ArrayList;
import java.util.Arrays;

public class Board {//TODO: ADD STALEMATE, 3 IDENTICAL STALEMATE, 50 move rule etc
    private Piece[][] boardStatus;
    private boolean currPromotion;
    private int[] wPawnTwo;
    private int[] bPawnTwo;
    private StringReader sr;
    private ArrayList<Piece> BPiece;
    private ArrayList<Piece> WPiece;

    private boolean wWin;
    private boolean bWin;
    //private ArrayList<Piece> BPieceCapt;
    //private ArrayList<Piece> WPieceCapt; //IS CAPT ARRAYLIST NECESSARY?
    private String currTurn;

    public Board() {
        boardStatus = new Piece[8][8];//white piece is gonna be on the bottom
        boardStatus[0][4] = new WKing(4, 0, boardStatus);
        boardStatus[0][0] = new WRook(0, 0, boardStatus);
        boardStatus[0][1] = new WKnight(1, 0, boardStatus);
        boardStatus[0][2] = new WBishop(2, 0, boardStatus);
        boardStatus[0][3] = new WQueen(3, 0, boardStatus);
        boardStatus[0][5] = new WBishop(5, 0, boardStatus);
        boardStatus[0][6] = new WKnight(6, 0, boardStatus);
        boardStatus[0][7] = new WRook(7, 0, boardStatus);

        boardStatus[7][4] = new BKing(4, 7, boardStatus);
        boardStatus[7][0] = new BRook(0, 7, boardStatus);
        boardStatus[7][1] = new BKnight(1, 7, boardStatus);
        boardStatus[7][2] = new BBishop(2, 7, boardStatus);
        boardStatus[7][3] = new BQueen(3, 7, boardStatus);
        boardStatus[7][5] = new BBishop(5, 7, boardStatus);
        boardStatus[7][6] = new BKnight(6, 7, boardStatus);
        boardStatus[7][7] = new BRook(7, 7, boardStatus);

        WPiece = new ArrayList<>();
        //WPieceCapt = new ArrayList<>();
        BPiece = new ArrayList<>();
        //BPieceCapt = new ArrayList<>();
        wPawnTwo = new int[8];
        bPawnTwo = new int[8];

        for (int i = 0; i < 8; ++i) {
            boardStatus[1][i] = new WPawn(i, 1, boardStatus, wPawnTwo, bPawnTwo);
            boardStatus[6][i] = new BPawn(i, 6, boardStatus, wPawnTwo, bPawnTwo);
        }

        for (int i = 0; i < 8; ++i) {
            WPiece.add(boardStatus[1][i]);
            WPiece.add(boardStatus[0][i]);
        }

        for (int i = 0; i < 8; ++i) {
            BPiece.add(boardStatus[7][i]);
            BPiece.add(boardStatus[6][i]);
        }
        currTurn = "W";
        sr = new StringReader();
        currPromotion = false;
        wWin = false;
        bWin = false;
    }

    public void reset() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                boardStatus[i][j] = null;
            }
        }
        boardStatus[0][4] = new WKing(4, 0, boardStatus);
        boardStatus[0][0] = new WRook(0, 0, boardStatus);
        boardStatus[0][1] = new WKnight(1, 0, boardStatus);
        boardStatus[0][2] = new WBishop(2, 0, boardStatus);
        boardStatus[0][3] = new WQueen(3, 0, boardStatus);
        boardStatus[0][5] = new WBishop(5, 0, boardStatus);
        boardStatus[0][6] = new WKnight(6, 0, boardStatus);
        boardStatus[0][7] = new WRook(7, 0, boardStatus);

        boardStatus[7][4] = new BKing(4, 7, boardStatus);
        boardStatus[7][0] = new BRook(0, 7, boardStatus);
        boardStatus[7][1] = new BKnight(1, 7, boardStatus);
        boardStatus[7][2] = new BBishop(2, 7, boardStatus);
        boardStatus[7][3] = new BQueen(3, 7, boardStatus);
        boardStatus[7][5] = new BBishop(5, 7, boardStatus);
        boardStatus[7][6] = new BKnight(6, 7, boardStatus);
        boardStatus[7][7] = new BRook(7, 7, boardStatus);

        for (int i = 0; i < 8; ++i) {
            boardStatus[1][i] = new WPawn(i, 1, boardStatus, wPawnTwo, bPawnTwo);
            boardStatus[6][i] = new BPawn(i, 6, boardStatus, wPawnTwo, bPawnTwo);
        }

        WPiece.clear();
        BPiece.clear();
        //WPieceCapt.clear();
        //BPieceCapt.clear();

        for (int i = 0; i < 8; ++i) {
            WPiece.add(boardStatus[1][i]);
            WPiece.add(boardStatus[0][i]);
        }

        for (int i = 0; i < 8; ++i) {
            BPiece.add(boardStatus[7][i]);
            BPiece.add(boardStatus[6][i]);
        }
        currTurn = "W";
    }

    public void clear() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                boardStatus[i][j] = null;
            }
        }
        WPiece.clear();
        BPiece.clear();
        //WPieceCapt.clear();
        //BPieceCapt.clear();
    }

    public void start() throws BlackKingNotFoundException, WhiteKingNotFoundException {
        WKing wking = null;
        BKing bking = null;

        for (Piece piece : WPiece) {
            piece.calcValidMoves();
            if (piece instanceof WKing) {
                wking = (WKing) piece;
            }
        }
        for (Piece piece : BPiece) {
            if (piece instanceof BKing) {
                bking = (BKing) piece;
            }
        }

        if (wking == null) {
            throw new WhiteKingNotFoundException();
        }
        if (bking == null) {
            throw new BlackKingNotFoundException();
        }
    }

    public boolean addPiece(String sCoord, PieceEnum pieceType) {
        int[] coord = sr.decipherCoord(sCoord);
        Piece pieceToAdd;
        if (boardStatus[coord[1]][coord[0]] != null) {
            return false;
        }
        switch (pieceType) {
            case BBishop:
                pieceToAdd = new BBishop(coord[0], coord[1], boardStatus);
                break;
            case BKing:
                pieceToAdd = new BKing(coord[0], coord[1], boardStatus);
                break;
            case BKnight:
                pieceToAdd = new BKnight(coord[0], coord[1], boardStatus);
                break;
            case BPawn:
                pieceToAdd = new BPawn(coord[0], coord[1], boardStatus, wPawnTwo, bPawnTwo);
                break;
            case BQueen:
                pieceToAdd = new BQueen(coord[0], coord[1], boardStatus);
                break;
            case BRook:
                pieceToAdd = new BRook(coord[0], coord[1], boardStatus);
                break;
            case WBishop:
                pieceToAdd = new WBishop(coord[0], coord[1], boardStatus);
                break;
            case WKing:
                pieceToAdd = new WKing(coord[0], coord[1], boardStatus);
                break;
            case WPawn:
                pieceToAdd = new WPawn(coord[0], coord[1], boardStatus, wPawnTwo, bPawnTwo);
                break;
            case WKnight:
                pieceToAdd = new WKnight(coord[0], coord[1], boardStatus);
                break;
            case WQueen:
                pieceToAdd = new WQueen(coord[0], coord[1], boardStatus);
                break;
            case WRook:
                pieceToAdd = new WRook(coord[0], coord[1], boardStatus);
                break;
            default:
                return false;
        }
        boardStatus[coord[1]][coord[0]] = pieceToAdd;
        if (pieceToAdd.getTeam().equals("W")) {
            WPiece.add(pieceToAdd);
        } else {
            BPiece.add(pieceToAdd);
        }
        return true;
    }

    public boolean removePiece(String sCoord) {
        int[] coord = sr.decipherCoord(sCoord);
        if (boardStatus[coord[1]][coord[0]] == null) {
            return false;
        }
        Piece pieceToRemove = boardStatus[coord[1]][coord[0]];
        if (pieceToRemove.getTeam().equals("W")) {
            WPiece.remove(pieceToRemove);
        } else {
            BPiece.remove(pieceToRemove);
        }
        boardStatus[coord[1]][coord[0]] = null;
        return true;
    }

    /**
     * Calc valid moves for current turn
     */

    public void printBoard() {
        for (int i = 7; i >= 0; --i) {
            for (int j = 0; j < 8; ++j) {
                if (boardStatus[i][j] != null) {
                    System.out.print(boardStatus[i][j] + " ");
                } else {
                    System.out.print("NAN ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public String getCurrTurn(){
        return currTurn;
    }

    public boolean getWWin(){
        return wWin;
    }

    public boolean getBWin(){
        return bWin;
    }
    public Piece[][] getBoardStatus() {
        return boardStatus;
    }

    public ArrayList<Piece> getBPiece() {
        return BPiece;
    }

    public ArrayList<Piece> getWPiece() {
        return WPiece;
    }

    /*public ArrayList<Piece> getBPieceCapt() {
        return BPieceCapt;
    }*/

    /*public ArrayList<Piece> getWPieceCapt() {
        return WPieceCapt;
    }*/

    public boolean movePiece(String sCommand) throws IncorrectPromotionPiece {
        int wLegalMoves = 0;
        int bLegalMoves = 0;
        if (!currPromotion) {
            int[] command;
            command = sr.decipherMove(sCommand);

            int xCoordIntBef = command[0];
            int yCoordIntBef = command[1];
            int xCoordIntAft = command[2];
            int yCoordIntAft = command[3];

            if (!notOutOfBound(xCoordIntBef, yCoordIntBef) || !notOutOfBound(xCoordIntAft, yCoordIntAft)) {
                return false;
            }

            Piece pieceToMove = boardStatus[yCoordIntBef][xCoordIntBef];
            Piece pieceAtDestination = boardStatus[yCoordIntAft][xCoordIntAft];

            if (pieceToMove == null) {
                return false;
            }

            if (!pieceToMove.getTeam().equals(currTurn)) {
                return false;
            }

            if (pieceToMove == null) {
                return false;
            }
            if (!pieceToMove.canMove(yCoordIntAft, xCoordIntAft)) {
                return false;
            }

            if (pieceToMove instanceof WKing) {//CASTLE
                if (xCoordIntAft == 6 && yCoordIntAft == 0) {
                    Piece wrook = boardStatus[0][7];
                    wrook.move(5, 0);
                    boardStatus[0][6] = pieceToMove;
                    boardStatus[0][5] = wrook;
                    boardStatus[0][4] = null;
                    boardStatus[0][7] = null;
                } else if (xCoordIntAft == 2 && yCoordIntAft == 0) {
                    Piece wrook = boardStatus[0][0];
                    wrook.move(3, 0);
                    boardStatus[0][2] = pieceToMove;
                    boardStatus[0][3] = wrook;
                    boardStatus[0][4] = null;
                    boardStatus[0][0] = null;
                }
            }

            if (pieceAtDestination != null) {
                capture(xCoordIntAft, yCoordIntAft);
            } else if (xCoordIntAft != xCoordIntBef) {//ENPASSANTE
                if (pieceToMove instanceof WPawn) {
                    capture(xCoordIntAft, yCoordIntAft - 1);
                } else if (pieceToMove instanceof BPawn) {
                    capture(xCoordIntAft, yCoordIntAft + 1);
                }
            }

            boardStatus[yCoordIntAft][xCoordIntAft] = pieceToMove;
            pieceToMove.move(xCoordIntAft, yCoordIntAft);
            boardStatus[yCoordIntBef][xCoordIntBef] = null;

            if ((pieceToMove instanceof WPawn && yCoordIntAft == 7) ||
                    (pieceToMove instanceof BPawn && yCoordIntAft == 0)) {
                currPromotion = true;
                return true;
            }
        } else {//Promotion implement
            int xCoordOfPro = 0;
            int yCoordOfPro = 0;
            for (int i = 0; i < 8; ++i) {
                if (boardStatus[7][i] instanceof WPawn) {
                    xCoordOfPro = boardStatus[7][i].getXCoord();
                    yCoordOfPro = boardStatus[7][i].getYCoord();
                    break;
                } else if (boardStatus[0][i] instanceof BPawn) {
                    xCoordOfPro = boardStatus[0][i].getXCoord();
                    yCoordOfPro = boardStatus[0][i].getYCoord();
                    break;
                }
            }

            char xCoordChar = (char) (xCoordOfPro + 'a');
            char yCoordChar = (char) (yCoordOfPro + '1');
            String stringCoord = xCoordChar + "" + yCoordChar;

            String pieceName = (currTurn + sCommand).toLowerCase();
            if (currTurn.equals("B")) {
                switch (pieceName) {
                    case "bqueen": {
                        addPiece(stringCoord, PieceEnum.BQueen);
                        break;
                    }
                    case "bbishop": {
                        addPiece(stringCoord, PieceEnum.BBishop);
                        break;
                    }
                    case "bknight": {
                        addPiece(stringCoord, PieceEnum.BKnight);
                        break;
                    }
                    case "brook": {
                        addPiece(stringCoord, PieceEnum.BRook);
                        break;
                    }
                    default:
                        throw new IncorrectPromotionPiece();
                }
            } else {
                switch (pieceName) {
                    case "wqueen": {
                        addPiece(stringCoord, PieceEnum.WQueen);
                        break;
                    }
                    case "wbishop": {
                        addPiece(stringCoord, PieceEnum.WBishop);
                        break;
                    }
                    case "wknight": {
                        addPiece(stringCoord, PieceEnum.WKnight);
                        break;
                    }
                    case "wrook": {
                        addPiece(stringCoord, PieceEnum.WRook);
                        break;
                    }
                    default:
                        throw new IncorrectPromotionPiece();
                }
            }

            currPromotion = false;
        }

        if (currTurn.equals("W")) {
            currTurn = "B";
        } else {
            currTurn = "W";
        }

        if (currTurn.equals("W")) {
            for (Piece piece : WPiece) {
                piece.calcValidMoves();
                wLegalMoves += piece.getPossibleMoves();
            }
            if(wLegalMoves == 0){
                bWin = true;
            }
            Arrays.fill(wPawnTwo, 0);
        } else {
            for (Piece piece : BPiece) {
                piece.calcValidMoves();
                bLegalMoves += piece.getPossibleMoves();
            }
            if(bLegalMoves == 0){
                wWin = true;
            }
            Arrays.fill(bPawnTwo, 0);
        }



        //printBoard();
        return true;
    }

    public boolean notOutOfBound(int xCoord, int yCoord) {
        return (xCoord >= 0 && xCoord < 8 && yCoord >= 0 && yCoord < 8);
    }

    public Piece getPiece(String sCoord) {
        int[] coordArr = sr.decipherCoord(sCoord);
        return boardStatus[coordArr[1]][coordArr[0]];
    }


    /*public boolean moveCommand(String moveCommandS){
        int[] moveCommandIndex = sr.decipherMove(moveCommandS);

        int xSourcePiece = moveCommandIndex[0];
        int ySourcePiece = moveCommandIndex[1];
        int xDestPiece = moveCommandIndex[2];
        int yDestPiece = moveCommandIndex[3];

        if(notOutOfBound(xSourcePiece, ySourcePiece) && notOutOfBound(xDestPiece, yDestPiece)) {
            Piece pieceToMove = boardStatus[ySourcePiece][xSourcePiece];

        }

        return false;
    }*/


    private void capture(int beCapturedX, int beCapturedY) {
        Piece pieceCaptured = boardStatus[beCapturedY][beCapturedX];
        if (pieceCaptured.getTeam().equals("W")) {
            WPiece.remove(pieceCaptured);
            //WPieceCapt.add(pieceCaptured);
            boardStatus[beCapturedY][beCapturedX] = null;
        } else {
            BPiece.remove(pieceCaptured);
            //BPieceCapt.add(pieceCaptured);
            boardStatus[beCapturedY][beCapturedX] = null;
        }
    }
}
