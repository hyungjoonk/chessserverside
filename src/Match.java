import java.net.Socket;
import java.net.ServerSocket;

public class Match {
    private int roomCode;
    private int playerCount = 0;
    private int spectatorCount = 0;
    private double timeIncrement;

    private double whiteTimeLeft, blackTimeLeft;
    private boolean matchHasStarted = false;

    private Board board;
    private String whiteName, blackName;
    private Socket whiteSocket, blackSocket;
    private String whiteIPAdress, blackIPAdress;//PROB DONT NEED


    public Match(int roomCode) {
        this.roomCode = roomCode;
        board = new Board();
    }

    public Match(int roomCode, double initialTime, double timeIncrement){
        this.roomCode = roomCode;
        this.blackTimeLeft = initialTime;
        this.whiteTimeLeft = initialTime;
        this.timeIncrement = timeIncrement;
        board = new Board();
    }

    public int getRoomCode() {
        return roomCode;
    }

    public void setWhiteName(String whiteName) {
        this.whiteName = whiteName;
    }

    public String getWhiteName() {
        return whiteName;
    }

    public void setBlackName(String blackName) {
        this.blackName = blackName;
    }

    public String getBlackName() {
        return blackName;
    }
    public void incrementPlayerCount(){
        ++playerCount;
    }

    public void decrementPlayerCount(){
        --playerCount;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setMatchHasStarted(boolean matchHasStarted){
        this.matchHasStarted = matchHasStarted;
    }

    public boolean getMatchHasStarted(){
        return matchHasStarted;
    }

    public void setTimeIncrement(double timeIncrement){
        this.timeIncrement = timeIncrement;
    }

    public double getTimeIncrement(){
        return timeIncrement;
    }

    public Socket getOtherSocket(Socket mySocket){
        if(mySocket.equals(whiteSocket)){
            return blackSocket;
        }
        else{
            return whiteSocket;
        }
    }


    public Board getBoard(){
        return board;
    }
    public void setWhiteIPAdress(String whiteIPAdress){
        this.whiteIPAdress = whiteIPAdress;
    }

    public String getWhiteIPAdress(){
        return whiteIPAdress;
    }

    public void setBlackIPAdress(String blackIPAdress){
        this.blackIPAdress = blackIPAdress;
    }

    public String getBlackIPAdress(){
        return blackIPAdress;
    }
    public void setWhiteSocket(Socket whiteSocket){
        this.whiteSocket = whiteSocket;
    }

    public Socket getWhiteSocket(){
        return whiteSocket;
    }

    public void setBlackSocket(Socket blackSocket){
        this.blackSocket = blackSocket;
    }

    public Socket getBlackSocket(){
        return blackSocket;
    }

    public void matchTerminate() {

    }

    public void matchRestart() {

    }

    public void changeMatchSetting() {

    }

    public void whiteWin() {

    }

    public void blackWin() {

    }
}
