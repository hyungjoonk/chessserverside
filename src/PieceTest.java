import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PieceTest {
    Piece pawn;
    Board board;

    Piece[][] boardStatus;
    @BeforeEach
    void setUp() {
        board = new Board();
        board.clear();
        boardStatus = board.getBoardStatus();
    }

    @Test
    void move() {
    }

    @Test
    void canMove() {
    }

    @Test
    void setCoord() {
    }

    @Test
    void getXCoord() {
    }

    @Test
    void getYCoord() {
    }

    @Test
    void hasMoved() {
    }

    @Test
    void getTeam() {
    }

    @Test
    void calcValidMoves() {
    }

    @Test
    void printValidMoves() {
        pawn.printValidMoves();
    }


    @Test
    void testKingUnderCheck(){
        board.addPiece("A3", PieceEnum.BBishop);
        board.addPiece("H8", PieceEnum.BKing);
        board.addPiece("C1", PieceEnum.WKing);
        WKing wking = (WKing)board.getBoardStatus()[0][2];
        BKing bking = (BKing)board.getBoardStatus()[7][7];
        board.printBoard();
        assertTrue(wking.isUnderCheck());
        board.removePiece("A3");
        board.printBoard();
        assertFalse(wking.isUnderCheck());
        board.addPiece("c8", PieceEnum.WRook);
        assertFalse(wking.isUnderCheck());
        assertTrue(bking.isUnderCheck());
        board.removePiece("c8");
        board.addPiece("c8", PieceEnum.BRook);
        assertTrue(wking.isUnderCheck());
        assertFalse(bking.isUnderCheck());
        board.printBoard();
        board.addPiece("c3", PieceEnum.WRook);
        board.removePiece("c8");

        WRook wrook = (WRook)board.getBoardStatus()[2][2];
        wrook.calcValidMoves();
        wrook.printValidMoves();
        board.printBoard();

        board.addPiece("c8", PieceEnum.BRook);
        wrook.calcValidMoves();
        wrook.printValidMoves();
        board.printBoard();

        board.addPiece("c2", PieceEnum.BPawn);
        wrook.calcValidMoves();
        wrook.printValidMoves();
        board.printBoard();

        board.clear();

        board.addPiece("A3", PieceEnum.BBishop);
        board.addPiece("H8", PieceEnum.BKing);
        board.addPiece("C1", PieceEnum.WKing);
        board.addPiece("D2", PieceEnum.WRook);
        WRook w2rook = (WRook)board.getBoardStatus()[1][3];

        w2rook.calcValidMoves();
        w2rook.printValidMoves();
        board.printBoard();
    }


    @Test
    void kingCheck(){
        board.addPiece("C3", PieceEnum.WKing);
        board.addPiece("H8", PieceEnum.BKing);

        board.printBoard();
        WKing wking = (WKing)boardStatus[2][2];
        BKing bking = (BKing)boardStatus[7][7];
        board.addPiece("B1", PieceEnum.BKnight);
        assertTrue(wking.isUnderCheck());
        board.removePiece(("B1"));
        assertFalse(wking.isUnderCheck());
        board.addPiece("B1", PieceEnum.WKnight);
        assertFalse(wking.isUnderCheck());
        board.printBoard();

        board.addPiece("a3", PieceEnum.BKing);
        bking = (BKing)boardStatus[2][0];
        assertTrue(bking.isUnderCheck());
        board.printBoard();
    }

    @Test
    void testQueenMovesAndBlockCheck(){
        board.addPiece("C3", PieceEnum.WKing);
        board.addPiece("H8", PieceEnum.BKing);

        WKing wking = (WKing)boardStatus[2][2];
        BKing bking = (BKing)boardStatus[7][7];

        board.addPiece("c7", PieceEnum.BRook);
        board.addPiece("e4", PieceEnum.WQueen);
        board.printBoard();

        WQueen wqueen = (WQueen)boardStatus[3][4];
        wqueen.calcValidMoves();
        wqueen.printValidMoves();
    }


    @Test
    void testEquals() {
    }
}