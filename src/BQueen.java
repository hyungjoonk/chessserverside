public class BQueen extends Piece {

    public BQueen(int xCoord, int yCoord, Piece[][] boardStatus) {
        super(xCoord, yCoord, "B", boardStatus);
        giveBkingRef();
    }


    @Override
    public void calcValidMoves() {
        if (bking.isUnderCheck()) {
            calcUnderCheck();
            return;
        }
        possibleMoves = 0;
        resetValidMoves();
        Piece objAtDest;
        int tempXCoord = xCoord;
        int tempYCoord = yCoord;

        ++tempYCoord;
        ++tempXCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); ++tempXCoord, ++tempYCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        ++tempXCoord;
        --tempYCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); ++tempXCoord, --tempYCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempXCoord;
        --tempYCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); --tempXCoord, --tempYCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempXCoord;
        ++tempYCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); --tempXCoord, ++tempYCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        //UP & DOWN

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        ++tempXCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); ++tempXCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempXCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); --tempXCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        ++tempYCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); ++tempYCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempYCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); --tempYCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }
    }

    private void calcUnderCheck() {
        //rook part
        possibleMoves = 0;
        resetValidMoves();
        Piece objAtDest;
        int tempXCoord = xCoord;
        int tempYCoord = yCoord;

        ++tempXCoord;
        for (; indexInRange(tempXCoord, tempYCoord); ++tempXCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }

                if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }


        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempXCoord;
        for (; indexInRange(tempXCoord, tempYCoord); --tempXCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }

        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        ++tempYCoord;

        for (; indexInRange(tempXCoord, tempYCoord); ++tempYCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempYCoord;
        for (; indexInRange(tempXCoord, tempYCoord); --tempYCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        //bishop part

        tempXCoord = xCoord;
        tempYCoord = yCoord;

        ++tempXCoord;
        ++tempYCoord;
        for (; indexInRange(tempXCoord, tempYCoord); ++tempXCoord, ++tempYCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }

                if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }


        tempXCoord = xCoord;
        tempYCoord = yCoord;
        ++tempXCoord;
        --tempYCoord;
        for (; indexInRange(tempXCoord, tempYCoord); ++tempXCoord, --tempYCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }

        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempYCoord;
        --tempXCoord;
        for (; indexInRange(tempXCoord, tempYCoord); --tempYCoord, --tempXCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        ++tempYCoord;
        --tempXCoord;
        for (; indexInRange(tempXCoord, tempYCoord); ++tempYCoord, --tempXCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if (!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

    }

    public String toString() {
        return "BQu";
    }
}