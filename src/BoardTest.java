import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class BoardTest {
    Board board;
    Piece piece;
    @BeforeEach
    void setUp() {
        board = new Board();
    }

    @Test
    void reset() {

    }

    @Test
    void calcValidMove() {

    }

    @Test
    void printBoard() {
        board.printBoard();
    }

    @Test
    void movePiece() {
    }

    @Test
    void myTest(){
        board.clear();
        System.out.println(board.addPiece("E1", PieceEnum.BKnight));
        board.printBoard();
    }

    @Test
    void generalIntegrationTest() throws IncorrectPromotionPiece{
        ArrayList<Piece> wpiece = board.getWPiece();
        System.out.println(board.movePiece("E2 E4"));
        board.printBoard();
        /*for(Piece piece : wpiece){
            piece.printValidMoves();
        }*/
        System.out.println(board.movePiece("D2 D4"));
        System.out.println(board.movePiece("D7 D5"));
        board.printBoard();

        piece = board.getPiece("E4");
        System.out.println(piece.getXCoord() + " " + piece.getYCoord());
        piece.calcValidMoves();
        piece.printValidMoves();

        System.out.println(board.movePiece("E4 D5"));
        board.printBoard();
    }

    @Test
    void enpassanteTest() throws IncorrectPromotionPiece{
        System.out.println(board.movePiece("E2 E4"));
        System.out.println(board.movePiece("D7 D5"));

        board.movePiece("E4 E5");
        //board.movePiece("D5 D4");

        //board.movePiece("E5 E6");
        board.movePiece("F7 F5");

        Piece piece = board.getPiece("E5");

        board.movePiece("E5 F6");

        board.movePiece("d5 d4");
        board.movePiece("c2 c4");

        piece = board.getPiece("d4");
        piece.calcValidMoves();
        piece.printValidMoves();

        board.movePiece("b8 c6");
        board.movePiece("h2 h4");
        piece.calcValidMoves();
        piece.printValidMoves();
    }
}