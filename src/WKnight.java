public class WKnight extends Piece{

    private int[][] validCoords;
    public WKnight(int xCoord, int yCoord, Piece[][] boardStatus){
        super(xCoord, yCoord, "W", boardStatus);
        validCoords = new int[8][2];
        validCoords[0][0] = 1;
        validCoords[0][1] = 2;
        validCoords[1][0] = 2;
        validCoords[1][1] = 1;
        validCoords[2][0] = 2;
        validCoords[2][1] = -1;
        validCoords[3][0] = 1;
        validCoords[3][1] = -2;
        validCoords[4][0] = -1;
        validCoords[4][1] = -2;
        validCoords[5][0] = -2;
        validCoords[5][1] = -1;
        validCoords[6][0] = -2;
        validCoords[6][1] = 1;
        validCoords[7][0] = -1;
        validCoords[7][1] = 2;
        giveWKingRef();
    }

    @Override
    public void calcValidMoves(){
        possibleMoves = 0;
        resetValidMoves();
        Piece objAtDest;
        int addXCoord;
        int addYCoord;
        for(int i = 0; i < 8; ++i){
            addXCoord = validCoords[i][0];
            addYCoord = validCoords[i][1];
            if(!indexInRange(xCoord + addXCoord, yCoord + addYCoord)){
                continue;
            }
            objAtDest = boardStatus[yCoord + addYCoord][xCoord + addXCoord];
            if(objAtDest != null && objAtDest.team.equals(team)){
                continue;
            }
            if(!moveLeaveKingCheck(xCoord + addXCoord, yCoord + addYCoord)){
                validMoves[yCoord + addYCoord][xCoord + addXCoord] = 1;
                ++possibleMoves;
            }
        }
    }

    public String toString(){
        return "WKn";
    }
}

