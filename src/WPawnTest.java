import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WPawnTest {
    Board board;
    WPawn pawn;

    @BeforeEach
    void setUp() {
        board = new Board();
        board.clear();
        board.addPiece("B2", PieceEnum.WPawn);
        //board.printBoard();
        pawn = (WPawn)board.getBoardStatus()[1][1];
    }

    @Test
    void calcValidMoves() {
        pawn.calcValidMoves();
        pawn.printValidMoves();
    }

    @Test
    void resetValidMoves() {
    }

    @Test
    void printValidMoves() {
    }
}